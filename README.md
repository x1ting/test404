project writted on Rails 4.2.1 and ruby 2.1.3

  Clone this repo 
```
#!bash

git clone git@bitbucket.org:x1ting/test404.git
```

  Run Bundler

```
#!bash

bundle install
```

  Create database.yml and secrets.yml file 


```
#!bash

rake secret # generate secret_key_base string for database.yml
```

  Create migrate and seed database. seed.rb configurated for creating 1000 students. If you want create more, change 28 line in db/seeds.rb file


```
#!bash

rake db:create db:migrate db:seed
```

  For search people with average ball from .. to .. and education in same semester, go to '/students/:id'