# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
start_time = Time.now
# Group's create
100.times do |i|
  Group.create(title: ('ist30' + i.to_s), semester_number: rand(9))
end

# Supervisor's create
text = "is a best supervisor on the world. He/she help me with my lover discipline"
["Ekaterina Ivanova", "Maria Vasileva", "Alexander Borisovich", "Petr Avstofeevich", "Nikolai Semenovich", "Fedor Borisovich", "Evgenyu Hlyzov"].each do |name|
  flag = rand(2)
  characheristic = flag.zero? ? (name + text) : nil
  Supervisor.create(name: name, characheristic: characheristic )
end

# Discipline's create
["Mathematic", "Language", "Biology", "Geometry", "Programming"].each do |title|
  Discipline.create(title: title)
end
ips = []
20.times do
  ips << IPAddr.new(rand(2**32),Socket::AF_INET)
end
# Student's create
1000.times do |i|
  start_student_time = Time.now
  name = (0...7).map { ('a'..'z').to_a[rand(26)] }.join
  surname = (0...15).map { ('a'..'z').to_a[rand(26)] }.join
  email = name + "@mail.ru"
  student = Student.create(name: name.capitalize,
                 surname: surname.capitalize,
                 group_id: Group.all.sample.id,
                 supervisor_id: Supervisor.all.sample.id,
                 birthday: Time.now - rand(200).week,
                 email: email,
                 ip_address: ips.sample.to_s)
  Discipline.all.each do |discipline|
    3.times do |i|
      Mark.create(student_id: student.id, discipline_id: discipline.id, value: rand(101))
    end
  end
  end_student_time = (Time.now - start_student_time)
  end_time = (Time.now - start_time).round
  puts "Created #{i} Student, \n Time for creating #{end_student_time} seconds \n General time expired #{end_time} seconds"
end
end_time = (Time.now - start_time).round
puts "Database seeded in #{end_time} seconds"
# select COUNT(*) as name from students as s inner join marks as m on m.student_id = s.id where ((select AVG (value) from marks) > 1)