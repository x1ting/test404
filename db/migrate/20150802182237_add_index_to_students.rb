class AddIndexToStudents < ActiveRecord::Migration
  def change
    add_index :students, :email
  end
end
