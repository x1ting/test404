class CreateMarks < ActiveRecord::Migration
  def change
    create_table :marks do |t|
      t.integer :student_id
      t.integer :discipline_id
      t.integer :value

      t.timestamps null: false
    end
  end
end
