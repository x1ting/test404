class AddIndexesToMarks < ActiveRecord::Migration
  def change
    add_index :marks, :student_id
    add_index :marks, :discipline_id
  end
end
