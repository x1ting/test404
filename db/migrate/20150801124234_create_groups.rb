class CreateGroups < ActiveRecord::Migration
  def change
    create_table :groups do |t|
      t.string :title
      t.integer :semester_number, default: 3

      t.timestamps null: false
    end
  end
end
