class Student < ActiveRecord::Base
  has_many :marks
  has_many :disciplines, through: :marks
  belongs_to :group
  belongs_to :supervisor
end
