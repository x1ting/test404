object @student

attributes :name, :surname, :birthday, :email, :ip_address

node :average do |student|
  student.marks.average("value").round(2)
end

node :semester_number do |student|
  student.group.semester_number
end
