json.array!(@students) do |student|
  json.extract! student, :id, :name, :surname, :group_id, :birthday, :email, :ip_address
  json.url student_url(student, format: :json)
end
