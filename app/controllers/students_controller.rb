class StudentsController < ApplicationController
  before_action :set_student, only: [:show, :edit, :update, :destroy]

  # GET /students
  # GET /students.json
  def index
    @students = Student.joins(:marks).group("students.id").having("avg(marks.value) > ?", 50).order("avg(marks.value) DESC").paginate(:page => params[:page])
  end

  def first_ten
    @students = Student.joins(:marks).group("students.id").order("avg(marks.value) DESC").limit(10)
    @students.includes(:disciplines)
    render 'students/first_ten'
  end

  def average_value
    set_student
    @students = Student.joins(:marks).joins(:group).where(groups: {semester_number: @student.group.semester_number}).group("students.id").having("avg(marks.value) < ? and avg(marks.value) > ?", params[:from].to_i, params[:to].to_i)
    render 'students/average_value'
  end

  def ip_address
    students = Student.includes(:supervisor, :group).all
    ip_addresses = Student.select("ip_address, count(ip_address)").group("students.ip_address").having("count(ip_address) > ?", 2)
    @students = []
    ip_addresses.each do |i|
      temp = students.select {|bb| bb.ip_address == i.ip_address}
      temp.each do |student|
        @flag = false
        if student.supervisor.characheristic.present?
          @flag = true
          break
        end
      end
      @students.push(temp) if @flag
    end
    @students.flatten!
    render 'students/ip_address'
  end



  # GET /students/1
  # GET /students/1.json

  def show
  end

  # GET /students/new
  def new
    @student = Student.new
  end

  # GET /students/1/edit
  def edit
  end

  # POST /students
  # POST /students.json
  def create
    @student = Student.new(student_params)

    respond_to do |format|
      if @student.save
        format.html { redirect_to @student, notice: 'Student was successfully created.' }
        format.json { render :show, status: :created, location: @student }
      else
        format.html { render :new }
        format.json { render json: @student.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /students/1
  # PATCH/PUT /students/1.json
  def update
    respond_to do |format|
      if @student.update(student_params)
        format.html { redirect_to @student, notice: 'Student was successfully updated.' }
        format.json { render :show, status: :ok, location: @student }
      else
        format.html { render :edit }
        format.json { render json: @student.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /students/1
  # DELETE /students/1.json
  def destroy
    @student.destroy
    respond_to do |format|
      format.html { redirect_to students_url, notice: 'Student was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_student
      @student = Student.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def student_params
      params.require(:student).permit(:name, :surname, :group_id, :birthday, :email, :ip_address)
    end
end
